//
//  MealPlanTableViewFirstCell.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/11/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

class MealPlanTableViewFirstCell: UITableViewCell {
    
    @IBOutlet weak var plusButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

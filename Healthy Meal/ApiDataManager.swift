//
//  ApiDataManager.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/8/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

protocol FoodApiDelegate {
    func didFinishGettingFoods(data:NSDictionary);
}

class ApiDataManager: NSObject {
    
    var foodDelegate : FoodApiDelegate?
    
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
    static let sharedInstance : ApiDataManager = {
        let instance = ApiDataManager();
        return instance;
    }();
    
    private override init() {
        super.init();
        
    }
    
    
    public func getFoodDataFromURLString(urlString: NSString) {
        guard let endpoint = URL(string: urlString as String) else {
            print("Error creating endpoint");
            return;
        }
        var dic = NSDictionary();
        URLSession.shared.dataTask(with: endpoint) { (data, response, error) in
            do {
                guard let data = data else {
                    throw JSONError.NoData
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                    throw JSONError.ConversionFailed
                }
                //print(json)
                dic = json as NSDictionary;
                DispatchQueue.main.async {
                    if let delegate = self.foodDelegate {
                        delegate.didFinishGettingFoods(data: dic);
                    }
                }
                //print(dic.object(forKey: "page") ?? "NOT OK");
            } catch let error as JSONError {
                print(error.rawValue)
            } catch let error as NSError {
                print(error.debugDescription)
            }
            }.resume()
    }
    
    public func setFoodDelegate(delegate: UIViewController!) {
        if let controller:UIViewController = delegate {
            self.foodDelegate = controller as? FoodApiDelegate;
        }
    }


}

//
//  FoodCollectionViewCell.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/8/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

class FoodCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!;
    @IBOutlet weak var nameLabel: UILabel!;
    @IBOutlet weak var kcaloryLabel: UILabel!;
    @IBOutlet weak var plusButton: UIButton!;
}

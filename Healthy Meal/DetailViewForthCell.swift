//
//  DetailViewForthCell.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/10/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

class DetailViewForthCell: UITableViewCell {
    
    @IBOutlet weak var dscriptionText: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

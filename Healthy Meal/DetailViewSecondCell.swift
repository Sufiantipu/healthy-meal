//
//  DetailViewSecondCell.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/8/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

class DetailViewSecondCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var kcaloryLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

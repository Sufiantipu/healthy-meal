//
//  MealViewFoodCell.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/12/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

class MealViewFoodCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var crossButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

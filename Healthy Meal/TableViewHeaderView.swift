//
//  TableViewHeaderView.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/10/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

class TableViewHeaderView: UIView {
    
    var dayNameLabel : UILabel!
    var imageView : UIImageView!
    var desLabel : UILabel!
    var downButton : UIButton!
    var headerNumber : NSInteger
    
    override init(frame: CGRect) {
        self.headerNumber = 0;
        super.init(frame: frame)
        self.createSubViews()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createSubViews() {
        self.setDaynameLabel()
        self.setImageView()
        self.setDesLabel()
        self.setDownButton()
    }
    
    func setDaynameLabel() {
        self.dayNameLabel = UILabel.init(frame: CGRect.init(x: 10, y: 10, width: self.frame.size.width / 2, height: 21));
        self.addSubview(self.dayNameLabel)
    }
    
    func setImageView() {
        self.imageView = UIImageView.init(frame: CGRect.init(x: 10, y: 41, width: 15, height: 15));
        self.imageView.image = UIImage.init(named: "Shape");
        self.addSubview(self.imageView)
    }
    
    func setDesLabel() {
        self.desLabel = UILabel.init(frame: CGRect.init(x: 45, y: 41, width: self.frame.size.width * 4 / 5 , height: 21));
        self.desLabel.center = self.imageView.center;
        self.desLabel.center.x = 45 + self.desLabel.frame.size.width / 2
        self.desLabel.font = UIFont.systemFont(ofSize: 10);
         self.addSubview(self.desLabel)
    }
    
    func setDownButton() {
        self.downButton = UIButton.init(frame: CGRect.init(x: self.frame.size.width - 20 - 29, y: self.frame.size.height / 2 - 15, width: 30, height: 30));
        self.downButton.setImage(UIImage.init(named: "Rectangle 10"), for: .normal)
        //self.downButton.addTarget(self, action: #selector(downButtonPressed(button:)), for: .touchUpInside)
         self.addSubview(self.downButton)
    }
    
    
    func downButtonPressed(button: UIButton) {
        
    }
    
    

}

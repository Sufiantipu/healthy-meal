//
//  DetailViewController.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/8/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    public var foodData:NSDictionary?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 4;
    }

    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0) {
            return self.view.frame.size.width * 276 / 375;
        } else if(indexPath.section == 1){
            return self.view.frame.size.width * 237 / 375;
        } else if(indexPath.section == 2) {
            return 190;
        } else if(indexPath.section == 3) {
            return 110;
        } else {
            return 0;
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 2 || section == 3) {
            return 20;
        } else {
            return 0;
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DetailViewFirstCell")!;
        if(indexPath.section == 0) {
            let cell:DetailViewFirstCell = tableView.dequeueReusableCell(withIdentifier: "DetailViewFirstCell") as! DetailViewFirstCell;
            
            let imageAray:NSArray = self.foodData!.object(forKey: "images") as! NSArray;
            let imageURLString:NSString = imageAray.object(at: 1) as! NSString;
            let url : NSURL = NSURL.init(string: imageURLString as String)!;
            cell.foodImageView.downloadedFrom(url: url as URL);
            cell.backgroundColor = UIColor.red;
            return cell;
        } else if(indexPath.section == 1) {
            let cell :DetailViewSecondCell = tableView.dequeueReusableCell(withIdentifier: "DetailViewSecondCell") as! DetailViewSecondCell;
            cell.titleLabel.text = foodData?.object(forKey: "name") as? String
            cell.kcaloryLabel.text = foodData?.object(forKey: "calorie") as? String
            return cell;
        } else if(indexPath.section == 2) {
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "DetailViewThirdCell")!;
            return cell;
        } else if(indexPath.section == 3) {
            let cell : DetailViewForthCell = tableView.dequeueReusableCell(withIdentifier: "DetailViewForthCell")! as! DetailViewForthCell;
            print(self.foodData?.object(forKey: "description") as? String ?? "kichu nai")
            cell.dscriptionText?.text = self.foodData?.object(forKey: "description") as? String;
            return cell;
        }
        return cell;
    }
    
    
    
    
    // MARK: - IBActions 
    
    @IBAction func backButtonPressed() {
        self.dismiss(animated: true, completion: nil);
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

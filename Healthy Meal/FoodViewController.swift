//
//  FoodViewController.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/8/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit

protocol FoodViewControllerDelegate {
    func didselectFood(food: NSDictionary, index: NSInteger)
}

class FoodViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, FoodApiDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    public var delegate: FoodViewControllerDelegate?
    var fromMealView: Bool = false
    var selectedSection = -1
    
    var foodArray : NSMutableArray = NSMutableArray();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setSearchBar();
        ApiDataManager.sharedInstance.foodDelegate = self;
        ApiDataManager.sharedInstance.getFoodDataFromURLString(urlString: "http://api-dev.akly.co/api/foods");
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public override func setTabBar() {
        let image : UIImage = (UIImage.init(named: "Home Icon Gray 2")?.withRenderingMode(.alwaysOriginal))!;
        let selectedImage: UIImage = (UIImage.init(named: "Home Icon Gray")?.withRenderingMode(.alwaysOriginal))!
        self.tabBarItem.title = "Home";
        self.tabBarItem.image = image;
        self.tabBarItem.selectedImage = selectedImage;
        self.tabBarController?.tabBar.tintColor = UIColor.gray;
    }
    
    
    func setSearchBar() {
        self.searchBar.backgroundImage = UIImage.init();
        self.searchBar.tintColor = UIColor.white;
    }
    
    //MARK: UICollection View Data Source
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return foodArray.count;
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:FoodCollectionViewCell;
        cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "foodCell", for: indexPath) as! FoodCollectionViewCell;
//        cell.nameLabel.text = "Apple";
        let dic : NSDictionary = foodArray.object(at: indexPath.row) as! NSDictionary;
        let imageAray:NSArray = dic.object(forKey: "images") as! NSArray;
        let imageURLString:NSString = imageAray.object(at: 1) as! NSString;
        let url : NSURL = NSURL.init(string: imageURLString as String)!;
        cell.imageView.downloadedFrom(url: url as URL);
        cell.imageView.layer.cornerRadius = 10;
        
        cell.nameLabel.text = dic.object(forKey: "name") as? String;
        let str:String =  dic.object(forKey: "calorie") as! String;
//        str.append("K")
        cell.kcaloryLabel.text = str;
        cell.plusButton.addTarget(self, action: #selector(plusButtonPressed(button:)), for: .touchUpInside)
        cell.plusButton.tag = 2000 + indexPath.row
        return cell;
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var width:CGFloat = self.collectionView.frame.size.width;
        width = 165 * self.collectionView.frame.size.width / 355;
        return CGSize.init(width: width, height: width * 217 / 165 + 10) ;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller:DetailViewController! = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController;
        if let VC = controller {
            VC.foodData = self.foodArray.object(at: indexPath.row) as? NSDictionary;
            self.present(VC, animated: true, completion: nil);
        }
    }
    
    // MARK - Button Actions
    
    func plusButtonPressed(button: UIButton) {
        if(self.fromMealView) {
            self.delegate?.didselectFood(food: (self.foodArray.object(at: button.tag - 2000) as? NSDictionary)!, index: self.selectedSection)
            self.fromMealView = false
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    // MARK - Food Delegate 
    
    func didFinishGettingFoods(data:NSDictionary) {
        let array:NSArray = data.object(forKey: "data") as! NSArray;
        self.foodArray.removeAllObjects();
        self.foodArray = NSMutableArray.init(array: array);
        self.collectionView.reloadData();
    }


}


extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleToFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleToFill) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

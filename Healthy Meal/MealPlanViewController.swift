//
//  MealPlanViewController.swift
//  Healthy Meal
//
//  Created by Md Abu Sufian on 6/10/17.
//  Copyright © 2017 Md Abu Sufian. All rights reserved.
//

import UIKit



class MealPlanViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, FoodViewControllerDelegate {

    var selectedSection: NSInteger = -1
    var openedSection: [Bool] = [false, false, false, false, false, false, false]
    
    var dalyMealArray1: NSMutableArray = NSMutableArray.init()
    var dalyMealArray2: NSMutableArray = NSMutableArray.init()
    var dalyMealArray3: NSMutableArray = NSMutableArray.init()
    var dalyMealArray4: NSMutableArray = NSMutableArray.init()
    var dalyMealArray5: NSMutableArray = NSMutableArray.init()
    var dalyMealArray6: NSMutableArray = NSMutableArray.init()
    var dalyMealArray7: NSMutableArray = NSMutableArray.init()
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public override func setTabBar() {
        let image : UIImage = (UIImage.init(named: "Gear Icon Gray 2")?.withRenderingMode(.alwaysOriginal))!;
        let selectedImage: UIImage = (UIImage.init(named: "Gear Icon Gray")?.withRenderingMode(.alwaysOriginal))!
        self.tabBarItem.title = "Home";
        self.tabBarItem.image = image;
        self.tabBarItem.selectedImage = selectedImage;
        self.tabBarController?.tabBar.tintColor = UIColor.gray;
    }
    
    // MARK: - Table View Delegate And Datasource Methods
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 7;
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var res = 0
        if(section == 0) {
            res += dalyMealArray1.count
        } else if(section == 1) {
            res += dalyMealArray2.count
        } else if(section == 2) {
            res += dalyMealArray3.count
        } else if(section == 3) {
            res += dalyMealArray4.count
        } else if(section == 4) {
            res += dalyMealArray5.count
        } else if(section == 5) {
            res += dalyMealArray6.count
        } else if(section == 6) {
            res += dalyMealArray7.count
        }
        return 1 + res;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(openedSection[indexPath.section]) {
            return 50;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view:TableViewHeaderView = TableViewHeaderView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 60));
        if(section == 0) {
            view.dayNameLabel.text = "SUNDAY";
        } else if(section == 1) {
            view.dayNameLabel.text = "MONDAY";
        } else if(section == 2) {
            view.dayNameLabel.text = "TUESDAY";
        } else if(section == 3) {
            view.dayNameLabel.text = "WEDNESDAY";
        } else if(section == 4) {
            view.dayNameLabel.text = "THURSDAY";
        } else if(section == 5) {
            view.dayNameLabel.text = "FRIDAY";
        } else if(section == 6) {
            view.dayNameLabel.text = "SATURDAY";
        }
        
        view.dayNameLabel.textColor = UIColor.init(colorLiteralRed: Float(((CGFloat)((0xF37E40 & 0xFF0000) >> 16))/255.0), green: Float(((CGFloat)((0xF37E40 & 0xFF00) >> 8))/255.0), blue: Float(((CGFloat)(0xF37E40 & 0xFF))/255.0), alpha: 1.0)
        
        view.desLabel.text = "400 kcal, Protein 50g, Carbs 20g, Fat 30g";
        view.desLabel.textColor = UIColor.gray
        view.headerNumber = section;
        view.downButton.addTarget(self, action: #selector(downButtonPressed(button:)), for: .touchUpInside)
        view.downButton.tag = 1000 + section;
        if(!openedSection[section]) {
             view.downButton.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2));
        } else {
            view.downButton.transform = CGAffineTransform(rotationAngle: 4 * CGFloat(Double.pi / 2));
        }
        return view;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0) {
            let cell: MealPlanTableViewFirstCell = tableView.dequeueReusableCell(withIdentifier: "MealPlanTableViewFirstCell") as! MealPlanTableViewFirstCell;
            cell.plusButton.addTarget(self, action: #selector(plusButtonPressed(button:)), for: .touchUpInside)
            cell.plusButton.tag = indexPath.section
            return cell;
        }
        let cell: MealViewFoodCell = tableView.dequeueReusableCell(withIdentifier: "MealViewFoodCell") as! MealViewFoodCell
        var array: NSMutableArray = NSMutableArray()
        if(indexPath.section == 0) {
            array = dalyMealArray1
        } else if(indexPath.section == 1) {
            array = dalyMealArray2
        } else if(indexPath.section == 2) {
            array = dalyMealArray3
        } else if(indexPath.section == 3) {
            array = dalyMealArray4
        } else if(indexPath.section == 4) {
            array = dalyMealArray5
        } else if(indexPath.section == 5) {
            array = dalyMealArray6
        } else if(indexPath.section == 6) {
            array = dalyMealArray7
        }
        
        let dic: NSDictionary = array.object(at: indexPath.row - 1) as! NSDictionary
        let imageAray:NSArray = dic.object(forKey: "images") as! NSArray;
        let imageURLString:NSString = imageAray.object(at: 1) as! NSString;
        let url : NSURL = NSURL.init(string: imageURLString as String)!;
        cell.iconImageView.downloadedFrom(url: url as URL)
        cell.nameLabel.text = dic.object(forKey: "name") as? String
        cell.crossButton.tag = 10000 + 1000 * indexPath.section + indexPath.row - 1;
        cell.crossButton.addTarget(self, action: #selector(crossButtonPressed(button:)), for: .touchUpInside)
        return cell
    }
    
     // MARK: - Cell Button Action
    
    func plusButtonPressed(button: UIButton) {
        selectedSection = button.tag + 1
        if let controller: FoodViewController = self.storyboard?.instantiateViewController(withIdentifier: "FoodViewController") as? FoodViewController {
            controller.delegate = self;
            controller.fromMealView = true
            controller.selectedSection = selectedSection
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func downButtonPressed(button: UIButton) {
        openedSection[button.tag - 1000] = !openedSection[button.tag - 1000]
        if(openedSection[button.tag - 1000]) {
            button.transform = CGAffineTransform(rotationAngle: 4 * CGFloat(Double.pi / 2));
        } else {
            button.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2));
        }
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    func crossButtonPressed(button: UIButton) {
        let section = (button.tag - 10000) / 1000;
        let row = (button.tag - 10000 - section * 1000);
        if(section == 0) {
           dalyMealArray1.removeObject(at: row)
        } else if(section == 1) {
            dalyMealArray2.removeObject(at: row)
        } else if(section == 2) {
            dalyMealArray3.removeObject(at: row)
        } else if(section == 3) {
            dalyMealArray4.removeObject(at: row)
        } else if(section == 4) {
            dalyMealArray5.removeObject(at: row)
        } else if(section == 5) {
            dalyMealArray6.removeObject(at: row)
        } else if(section == 6) {
            dalyMealArray7.removeObject(at: row)
        }
        self.tableView.reloadData()
    }
    
    // MARK: - Food View Controller Delegate
    
    func didselectFood(food: NSDictionary, index: NSInteger) {
        if(index == 1) {
            dalyMealArray1.add(food)
        } else if(index == 2) {
            dalyMealArray2.add(food)
        } else if(index == 3) {
            dalyMealArray3.add(food)
        } else if(index == 4) {
            dalyMealArray4.add(food)
        } else if(index == 5) {
            dalyMealArray5.add(food)
        } else if(index == 6) {
            dalyMealArray6.add(food)
        } else if(index == 7) {
            dalyMealArray7.add(food)
        }
        self.tableView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
